TODO: Consider how this process is affected by different roles: cook, captain, deckhand, etc.

* [ ]  Nextcloud Account
* [ ]  Zimbra Account
* [ ]  value-stream briefing: what we mean by Plan, Promote, Prep, etc...
* [ ]  Gitlab.com Role
* [ ]  Review Proxmox Access
* [ ]  Git local customs (push to branch & pr vs push to master)