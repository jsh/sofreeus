#### Checklist

- [ ] Create a "post" issue using the `post-an-event` template
- [ ] Create a "prep" issue using the `prep-an-event` template
- [ ] Create a "promote" issue using the `promote-an-event` template
- [ ] Create a "Heather's Kitchen" issue using the `pfood-an-event` template
- [ ] Create a "payout" issue using the `payout-an-event` template
- [ ] Add the event to [the schedule](/schedule.md), filling out *all* columns

##### Use this pattern for issue naming:

`<P-word> <M/D>: <class-single-word-name> w <teacher>`

Example:

`Promote 4/20: ZickleTwister w Shoup`
