# SFS Past Events

See also: [The Future](schedule.md)

| Date          | Subject   | Location     | Teacher           | Notes          | Post | Promote | Payout |
| ------------- | --------- | ------------ | ----------------- | -------------- | ---- | ------- | ------ |
| **January**   |
| 2019-01-05    | AGHI2 DNS | CodeTalent   | Chris Fedde |  | [Done](https://www.meetup.com/sofreeus/events/257003378/) | Failed #521 | #542 #543 #544 #545 |
| 2019-01-19    | Python 101: Structure and Tools | CodeTalent  | Rich Glazier |  | [Done](https://www.meetup.com/sofreeus/events/257897123/) | [Done](https://twitter.com/DavidLWillson/status/1085366657038508032) |  |
| **February**  |
| 2019-02-02    | Python 102 | CodeTalent  | Zac Champion |  | [Done](https://www.meetup.com/sofreeus/events/257897395/) | [Done](https://twitter.com/DavidLWillson/status/1088232738841186304) |  |
| 2019-02-16    | Intro to DevOps | CodeTalent  | Silvia, Anthony, and Kim |  | [Done](https://www.meetup.com/sofreeus/events/257482237/) | [Done](https://twitter.com/SFS303/status/1093170585595723776) |  |
| 2019-02-19    | SFS Method | Comfy Room & Zoom | David L. Willson |  | [Done](https://www.meetup.com/sofreeus/events/258342461/) | [Done](https://twitter.com/SFS303/status/1092824578420727808) |  |
| **March**     |
| 2019-03-02    | AGHI2 Docker | CodeTalent | Duncan Fedde |  | [Done](https://www.meetup.com/sofreeus/events/258870303/) | [Done](https://twitter.com/SFS303/status/1095524551197257728) |  |
| 2019-03-16    | Small is beautiful | CodeTalent | DLW, Shawn, Rich, Derek, Silvia, Alex, JSH | RMS's birthday - small classes - less than an hour each | [Done](https://www.meetup.com/sofreeus/events/259297394/) | [Done](https://twitter.com/SFS303/status/1100427361433079808) |  |
| **April**     |
| 2019-04-06    | Python 201 | CodeTalent | Aaron Brown |  | #576 | #628 |  |
| 2019-04-20    | Kubernetes | CodeTalent | Duncan Fedde |  | #361 |  |  |
| **May**       |
| 2019-05-04    | GitLab | CodeTalent | David L. Willson |  | [Done](https://www.meetup.com/sofreeus/events/257902517) | #622 |  |
| 2019-05-18    | Intro to DevOps |  | Anthony, Kim, Silvia, Aaron |  | #626 |  |  |
| 2019-05-29    | SFS Method | Comfy Room & Zoom | David L. Willson | 6pm to 10pm | #624 |  |  |
| **June**      |
| 2019-06-01    | Hacking Hours (informal) |  |  |  |  |  |  |
| 2019-06-15    | Ansible 101 |  | Alex Wise | #647 | #630 | oops |  |
| **July**      |
| 2019-07-06    | Penetrations and Remediations | Code | Mike Harris |  | [done](https://www.meetup.com/sofreeus/events/261632782/) | [done](https://twitter.com/SFS303/status/1145029127390552071) |  |
| 2019-07-20    | Prometheus | Code | Mike Shoup |  | [done](https://www.meetup.com/sofreeus/events/260918433/) | [done](https://twitter.com/shouptech/status/1142507095104737280) |  |
| **August**    |
| 2019-08-03    | nginx | Code | Silvia | #689 | #687 | #688 |  |
| 2019-08-17    | small is beautiful | Comfy Room | David L. Willson | #691 | #690 | #692 |  |
| **September** |
| 2019-09-07    | SFS Method | Comfy Room | Mike Shoup and DLW | #728 | #729 | #730 |  |
| 2019-09-21    | Continuous Integration with Gitlab CI | Code | Aaron Brown | #693 | #694 | #695 |  |
| **October**   |
| 2019-10-04    | AGHI2 OpenStack | CodeTalent | CRF and DLW | #731 | #732 | #733 |  |
| 2019-10-18    | Intro to DevOps (3rd run) | CodeTalent | Aaron, Anthony, Silvia | #734 | #700 | #735 |  |
| **November**  |
| 2019-11-02    |  |  |  |  |  |  |  |
| 2019-11-16    | Nextcloud: Share it your way | CodeTalent | Mike Shoup | #767 | #765 | #766 | #768 |
| **December**  |
| 2019-12-07    | AGHI2 ProxMox | Comfy Room | TRR and DLW |  |  |  |  |
| 2019-12-21    | Holiday Open House | Comfy Room | Heather | NA | NA | NA | NA |
| 2019-12-28    | Linus' Birthday |  |  |  |  |  |  |
| **January**   |
| 2020-01-04    | SIB | Code | Ed, DLW | #830 | https://www.meetup.com/sofreeus/events/267112354/ | #831 | #832 |
| 2020-01-13    | LFCS SG | Comfy Room & Zoom | Garheade and DLW | #772 | https://www.meetup.com/sofreeus/events/brlxlrybccbrb/ | #779 | #833 |
| 2020-01-17    | CU I2DO | CU | Anthony & Aaron | #790 #839 | n/a | n/a | #835 |
| 2020-01-17    | No class - recovery day |  |  |  |  |  |  |
| **February**  |
| 2020-02-01    | 2020 Strategy Meeting |  |  |  |  |  |  |
| 2020-02-15    | Intro to DevOps | Code Talent | ??? | #795 ? #816 | #815 | #840 | #841 |
| **March**     |
| 2020-03-07    | Pi Fight| | | | |  |  |
| 2020-03-14    | Pi Day |  |  |  |  |  |  |
| 2020-03-16    | RMS' Birthday |  |  |  |  |  |  |
| 2020-03-21    | Intro to Docker | Online | tbd | #808 | #807 | #809 | #810 |
| **April**     |
| 2020-04-04    | Ansible | Online | David | #826 | #827 | #828 | #829 |
| 2020-04-18    | Featureban | Online | Ed |  |  |  |  |
| **May**       |
| 2020-05-02    | Stencyl | Online | Conor B. | #849 | #850 | #851 | #852 |
| 2020-05-06    | RHCSA CSG | Online | Mark Sallee and DLW |  |  |  |  |
| 2020-05-16    | Haproxy | Online | DLW |  | https://www.meetup.com/sofreeus/events/270351178/ |  |  |
| **June**      |
| 2020-06-06    | Hacking Hours + Meetup API Hackathon | Online | Alex W |  |  |  |  |
| 2020-06-20    | Hacking Hours | Online | DLW | #909 | #910 | #911 | NA |
| 2020-06-27    | KTHW |  |  |  |  |  |  |
| **July**      |
| 2020-07-11    | OpenShift | Online | Silvia | #905 | #906 | #907 | #908 |
| 2020-07-18    | Intro to DevOps | Online | Anthony, Aaron, Silvia | #901 | #900 | #902 | #903 |
| **August**    |
| 2020-08-08    | Hacking Hours | online | Willson | --- | https://www.meetup.com/sofreeus/events/272387182/ | --- | --- |
| 2020-08-22    | Hacking Hours | online | Willson | --- | https://www.meetup.com/sofreeus/events/272387196/ | --- | --- |
| 2020-08-29    | Start Streaming or Step Up Your Meetings with OBS | Online | Ed Schaefer | #921 | #917 | #923 | #924 |
 **September** |
| 2020-09-10    | Python Study Group (8 weeks) | Online | Peer Led | #948 |  | #950 |
| 2020-09-19    | OpenStack | Online | DLW | #948 | https://www.meetup.com/sofreeus/events/273214887/ | #950 | #951 |
| **October**   |
| 2020-10-03    | Prometheus, Grafana, and Friends | Online | Mike Shoup, Justin Lang, and DLW | #912 | #913 | #914 | #915 |
| 2020-10-17    | CANCELED Intro to DevOps | Online | Aaron and Anthony | #943 | #944 | #945 | #946 |
| **November**  |
| 2020-11-07    | CANCELED DevSecOps Beta |  | Anthony and ??? |  |  |  |  |
| 2020-11-21    | CANCELED DevSecOps Round 1 |  | Anthony and ??? |  |  |  |  |
| **December**  |
| 2020-12-05    | Proxmox | Online | Troy Ridgley (and DLW) | #963 | #964 | #965 | #966 |
| 2020-12-19    | Year Retro + Thank You Dinner |  |  |  |  |  |  |
