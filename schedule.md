# SFS Event Calendar

See also: [The Past](schedule-past.md)

| Date          | Subject   | Location     | Teacher           | Prep | Post | Promote | Payout |
| ------------- | --------- | ------------ | ----------------- | ---- | ---- | ------- | ------ |
| **September** |
| 2021-09-04 | A is for Ansible | hybrid | DLW | [x] | https://www.meetup.com/sofreeus/events/280489199/ | [x] | [x] |
| 2021-09-11 | B is for Bash | hybrid | DLW | [x] | https://www.meetup.com/sofreeus/events/280667557/ | [x] | [ ] |
| 2021-09-18 | C is for C  | hybrid | BG |  | https://www.meetup.com/sofreeus/events/280780284/ | [x] | [ ] |
| 2021-09-23 | D is for Docker | hybrid | DLW | [x] | https://www.meetup.com/sofreeus/events/280923547/ | [x] | [ ] |
| 2021-09-25 | D is for Data Structures | online | KD | [ ] | https://www.meetup.com/sofreeus/events/280928738 | [ ] | [ ] |
| 2021 week 39 | E is for ... |
| ? | Everything and Everyone | online | DLW | [ ] | [ ] | [ ] | [ ] |
| ? | Encryption | online | Viktoria | [ ] | [ ] | [ ] | [ ] |
| 2021 week 40 | F is for ... |
| ? | Freedom | online | DLW | [ ] | [ ] | [ ] | [ ] |
| 2021 week 41 | G is for ... |
| 2021-10-16 | Version Control | hybrid | JSH| [ ] | [ ] | [ ] | [ ] |
| ? | Games: Mindustry, iFrac, gnomine, +RasPi list  | hybrid | DLW| [ ] | [ ] | [ ] | [ ] |
| 2021 week 42 | H is for ... |
| ? | Helm | - | ? | [ ] | [ ] | [ ] | [ ] |
| ? | Help - 5FDP | hybrid | DLW and GAR| [ ] | [ ] | [ ] | [ ] |
| 2021 week 43 | I is for ... |
| ? | Infrastructure | - | ?| [ ] | [ ] | [ ] | [ ] |
| 2021 week 44 | J is for ... |
| ? | Jaeger | ? | ACW | [ ] | [ ] | [ ] | [ ] |
| 2021 week 45 | K is for ... |
| 2021 week 46 | L is for ... |
| ? | Linux | ? | GAR | [ ] | [ ] | [ ] | [ ] |
| 2021 week 47 | M is for ... |
| ? | Monitoring | ? | ? | [ ] | [ ] | [ ] | [ ] |
| 2021 week 48 | N is for ... |
| 2021 week 49 | O is for ... |
| ? | OpenTelemetry | ? | ACW | [ ] | [ ] | [ ] | [ ] |
| 2021 week 50 | P is for ... |
| 2021 week 51 | Q is for ... |
| 2021 week 52 | R is for ... |
| 2022 week 01 | S is for ... |
| 2022 week 02 | T is for ... |
| 2022 week 03 | U is for ... |
| 2022 week 04 | V is for ... |
| 2022 week 05 | W is for ... |
| 2022 week 06 | X is for ... |
| 2022 week 07 | Y is for ... |
| 2022 week 08 | Z is for ... |
| ? | zShell | ? | GAR| [ ] | [ ] | [ ] | [ ] |
