# Upcoming Events

Would you rather view the Upcoming Events in your browser? [Click here](https://gitlab.com/sofreeus/sofreeus/blob/master/upcoming-events.md)!


## Highlights
* Happy Year's end friends!  Software Freedom School is grateful to you for attending classes, teaching classes, and helping us navigate through a unique time. 
*  SFS is working on learning tracks for 2021.  This should help keep you motivated to learn, socialize, and be even more brilliant than you already are. :)
* If there is a link in the Meetup posting for a sign in sheet PLEASE take a minute or two to fill it out.  It really helps us!
* Help promote our classes by sharing the Meetup links on our FB (https://www.facebook.com/softwarefreedomschool/?view_public_for=1409254222735496), LinkedIn (ask to link with me linkedin.com/in/heatherwillson), or resharing on our twitter account (SFS303)

---

# UPCOMING CLASSES

---

# ONLINE: 2020-12-19 Cooking class with Heather L. Willson! 

I am SO excited to be cooking with you! We are going to make Almost Vegan Figgy Pizza. Don't let the name scare you - it IS delicious!

I will be making the recipe Gluten Free and it has not been tested with All-Purpose flour. It's a toothsome crust with fancy toppings. If you're looking for a regular pizza... this ain't it!

Once you sign up I'll send you the recipe and shopping list.

Cost of class: donations only (you can send to my PayPal account)
You will need to provide your own ingredients.

Hope to see you!

https://www.meetup.com/sofreeus/events/274959502/

---

If you don't see what you want, send your wish to captains@sofree.us! We definitely take requests!

## Reminders

* We are lifelong learners. Please share learning opportunities, even if they're not produced by SFS.
* We are friends. You can post just about anything to the [ML](http://lists.sofree.us/cgi-bin/mailman/listinfo/sfs). If it's not free-libre, put "OT:" in the subject.
* We are professionals. Your "help wanted" and "job wanted" posts are welcome.
* Come hang out with us in [Mattermost](https://mattermost.sofree.us/sfs303/channels/town-square)!
